/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.animatedframes.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.RegisteredListener;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class ImageLoadEvent extends Event {

	private URL        url;
	private int        width;
	private int        height;
	private JSONObject meta;

	private LoadCallback loadCallback;
	private              boolean             called                = false;
	private static final Set<ImageLoadEvent> pendingEventCallbacks = new HashSet<>();

	public ImageLoadEvent(URL url, int width, int height, JSONObject meta, LoadCallback callback) {
		this.url = url;
		this.width = width;
		this.height = height;
		this.meta = meta;
		this.loadCallback = callback;

		synchronized (pendingEventCallbacks) {
			pendingEventCallbacks.add(this);
		}
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public JSONObject getMeta() {
		return meta;
	}

	public void setMeta(JSONObject meta) {
		this.meta = meta;
	}

	/**
	 * Only call if a value was modified!
	 * <p>
	 * calls the callback and loads the image
	 * (can only be called once)
	 */
	public void finish() {
		if (called) { return; }
		this.loadCallback.call(getUrl(), getWidth(), getHeight(), getMeta());
		pendingEventCallbacks.remove(this);
		called = true;
	}

	/**
	 * Call this if no modifications were made
	 * <p>
	 * calls the callback if only one listener is registered, otherwise waits for the next listener to finish or ignore
	 */
	public void ignore() {
		RegisteredListener[] listeners = getHandlers().getRegisteredListeners();
		if (listeners != null) {
			if (listeners.length <= 1) {//Only a single registered listener (i.e. the caller of this method): we can finish
				finish();
			}
			//More listeners: wait for more calls to finish/ignore
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		ImageLoadEvent event = (ImageLoadEvent) o;

		if (width != event.width) { return false; }
		if (height != event.height) { return false; }
		if (called != event.called) { return false; }
		if (url != null ? !url.equals(event.url) : event.url != null) { return false; }
		if (meta != null ? !meta.equals(event.meta) : event.meta != null) { return false; }
		return !(loadCallback != null ? !loadCallback.equals(event.loadCallback) : event.loadCallback != null);

	}

	@Override
	public int hashCode() {
		int result = url != null ? url.hashCode() : 0;
		result = 31 * result + width;
		result = 31 * result + height;
		result = 31 * result + (meta != null ? meta.hashCode() : 0);
		result = 31 * result + (loadCallback != null ? loadCallback.hashCode() : 0);
		result = 31 * result + (called ? 1 : 0);
		return result;
	}

	public static Set<ImageLoadEvent> getPendingEventCallbacks() {
		synchronized (pendingEventCallbacks) { return new HashSet<>(pendingEventCallbacks); }
	}

	public static int getPendingCallbacks() {
		int i = 0;
		for (ImageLoadEvent event : getPendingEventCallbacks()) {
			i += event.getHandlers().getRegisteredListeners().length;
		}
		return i;
	}

	private static HandlerList handlerList = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

	public static HandlerList getHandlerList() {
		return handlerList;
	}

	public interface LoadCallback {
		void call(URL url, int width, int height, JSONObject meta);
	}
}
