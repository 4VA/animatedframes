/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.animatedframes;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.plugin.RegisteredListener;
import org.bukkit.plugin.java.JavaPlugin;
import org.inventivetalent.animatedframes.event.ImageLoadEvent;
import org.inventivetalent.animatedframes.hooks.FramePictureHook;
import org.inventivetalent.animatedframes.util.ImageUtil;
import org.inventivetalent.animatedframes.util.URLHelper;
import org.inventivetalent.mapmanager.ArrayImage;
import org.inventivetalent.mapmanager.MapManagerPlugin;
import org.inventivetalent.mapmanager.manager.MapManager;
import org.inventivetalent.mapmanager.wrapper.MapWrapper;
import org.inventivetalent.update.spigot.SpigotUpdater;
import org.json.JSONObject;
import org.mcstats.MetricsLite;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.logging.Level;

public class AnimatedFrames extends JavaPlugin implements Listener {

	String prefix = "§6[§eAnimatedFrames§6]§r ";

	public static AnimatedFrames instance;

	protected double  animationDistance = 32.0D;
	protected boolean frameDebug        = false;
	protected boolean enableTimings     = true;
	protected boolean loadingImage      = true;
	protected boolean ignoreSlowLoading = false;

	protected boolean console_chunk = true;

	List<AnimatedImage> frames;

	private Map<UUID, JSONObject> injectingMeta;
	private Map<UUID, URL>        creatingFrame;
	private List<UUID>            removingFrame;
	private Map<UUID, Dimension>  resizeFrame;

	ScriptEngineManager scriptEngineManager;
	public ScriptEngine scriptEngine;

	private static AnimatedFramesAPI api;

	protected ArrayImage loadingArrayImage;
	protected MapWrapper loadingWrapper;

	@Override
	public void onLoad() {
		api = new API();
	}

	@Override
	public void onEnable() {
		if (!Bukkit.getPluginManager().isPluginEnabled("MapManager") || !Bukkit.getPluginManager().isPluginEnabled("PacketListenerApi")) {
			getLogger().severe("*****************************************************");
			getLogger().severe("This plugin depends on MapManager & PacketListenerAPI");
			getLogger().severe("Download here: https://r.spiget.org/2930/19198");
			getLogger().severe("*****************************************************");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		instance = this;

		Bukkit.getPluginManager().registerEvents(this, this);

		frames = new ArrayList<>();
		injectingMeta = new HashMap<>();
		creatingFrame = new HashMap<>();
		removingFrame = new ArrayList<>();
		resizeFrame = new HashMap<>();

		AFTimings.init();

		this.saveDefaultConfig();

		//Check deprecated&moved config options
		String[] deprecatedConfig = new String[] {
				"idAddition",
				"ignoredIDs",
				"console.optimization" };
		String[] movedConfig = new String[] {
				"mapSend.delay",
				"mapSend.amount" };
		for (String deprecated : deprecatedConfig) {
			if (getConfig().contains(deprecated)) {
				getLogger().warning("'" + deprecated + "' is no longer used");
			}
		}
		for (String moved : movedConfig) {
			if (getConfig().contains(moved)) {
				getLogger().warning("'" + moved + "' has been moved to the MapManager plugin configuration");
			}
		}
		reload();

		scriptEngineManager = new ScriptEngineManager();
		scriptEngine = scriptEngineManager.getEngineByName("JavaScript");

		File frameFolder = new File(this.getDataFolder(), "/frames");
		if (!frameFolder.exists()) {
			frameFolder.mkdirs();
		}

		File loadingImage = new File(getDataFolder(), "af_loading.png");
		if (!loadingImage.exists()) {
			saveResource("af_loading.png", false);
		}
		if (this.loadingImage) {
			try {
				BufferedImage image = ImageIO.read(loadingImage);
				if (image == null) {
					getLogger().warning("Failed to read loading image");
					this.loadingImage = false;
				} else if (image.getWidth() != 128 || image.getHeight() != 128) {
					getLogger().warning("Loading image must be 128x128 pixels");
					this.loadingImage = false;
				} else {
					loadingArrayImage = new ArrayImage(image);
				}
			} catch (Exception e) {
				getLogger().log(Level.WARNING, "Failed to read loading image", e);
				this.loadingImage = false;
			}
			MapManager mapManager = ((MapManagerPlugin) Bukkit.getPluginManager().getPlugin("MapManager")).getMapManager();
			loadingWrapper = mapManager.wrapImage(loadingArrayImage);
		}

		if (Bukkit.getPluginManager().isPluginEnabled("FramePicture")) {
			getLogger().info("Found FramePicture plugin");
			FramePictureHook.enable();
		}

		try {
			MetricsLite metrics = new MetricsLite(this);
			if (metrics.start()) {
				getLogger().info("Metrics started.");
			}

			new SpigotUpdater(this, 5583);
		} catch (Exception e) {
		}

		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
			@Override
			public void run() {
				if (isEnabled()) {
					if (loadFrames()) {
						getLogger().info("Successfully loaded frames.");
					}
				}
			}
		}, 80);
		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
			@Override
			public void run() {
				if (!ImageLoadEvent.getPendingEventCallbacks().isEmpty()) {
					getLogger().warning("There are " + ImageLoadEvent.getPendingCallbacks() + " load listeners that have not finished loading yet");

					for (ImageLoadEvent event : ImageLoadEvent.getPendingEventCallbacks()) {
						getLogger().info(event.toString());
						for (RegisteredListener listener : event.getHandlers().getRegisteredListeners()) {
							getLogger().info("- " + listener.getPlugin() + " @ " + listener.getListener());
						}
					}

					if (ignoreSlowLoading) {
						getLogger().info("Ignoring load listeners");
					} else {
						getLogger().info("Forcing load");
						for (ImageLoadEvent event : ImageLoadEvent.getPendingEventCallbacks()) {
							event.finish();
						}
					}
				}
			}
		}, 20 * 60);
	}

	void reload() {
		reloadConfig();

		AnimatedImage.SINGLE_IMAGE_DELAY = Math.max(getConfig().getInt("mapSend.single.delay", AnimatedImage.SINGLE_IMAGE_DELAY), 1);

		animationDistance = getConfig().getDouble("animationDistance", animationDistance);
		frameDebug = getConfig().getBoolean("frameDebug", frameDebug);
		enableTimings = getConfig().getBoolean("enableTimings", enableTimings);
		loadingImage = getConfig().getBoolean("loadingImage", loadingImage);
		ignoreSlowLoading = getConfig().getBoolean("ignoreSlowLoading", ignoreSlowLoading);

		console_chunk = getConfig().getBoolean("console.chunk_unload", console_chunk);
	}

	@Override
	public void onDisable() {
		for (AnimatedImage img : frames) {
			img.stopAnimation();
		}

		if (this.saveFrames()) {
			frames.clear();
		}
	}

	/**
	 * @return The API-instance
	 */
	public static AnimatedFramesAPI getApi() {
		return api;
	}

	//TODO: /af reloadframes command (save & load frames again)
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("animatedframes") || label.equalsIgnoreCase("animframes") || label.equalsIgnoreCase("af") || label.equalsIgnoreCase("aframes") || label.equalsIgnoreCase("animfrms")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(prefix + "§cCommand is only available for players!");
				return false;
			}
			Player p = (Player) sender;
			if (args.length >= 1) {
				if (args[0].equalsIgnoreCase("create")) {
					if (!sender.hasPermission("animatedframes.create")) {
						sender.sendMessage(prefix + "§cNo permission.");
						return false;
					}

					if (creatingFrame.containsKey(p.getUniqueId())) {
						sender.sendMessage(prefix + "§cYou are already creating a frame!");
						return false;
					}

					if (args.length == 1) {
						sender.sendMessage(prefix + "§c/animatedframes create <URL> [width] [height]");
						return false;
					}
					String urlString = args[1];
					URL url = null;
					try {
						url = new URL(urlString);
					} catch (MalformedURLException e) {
						try {
							File file = new File(urlString);
							if (!file.exists()) { throw new FileNotFoundException(); }
							url = file.toURI().toURL();
						} catch (FileNotFoundException | MalformedURLException e1) {
							sender.sendMessage(prefix + "§cPlease enter a valid URL or File path!");
							return false;
						}
					}

					//Java might mess up JPEG images (http://stackoverflow.com/questions/13072312/jpeg-image-color-gets-drastically-changed-after-just-imageio-read-and-imageio#answer-20980628)
					if (!ImageUtil.isValidImage(url.toString())) {
						sender.sendMessage("§eCould not validate image type. Make sure to use .gif or .png (.jpg/.jpeg are only partially supported)");
					}

					if (args.length == 3) {
						int percentage;
						try {
							percentage = Integer.parseInt(args[2]);
						} catch (NumberFormatException e) {
							sender.sendMessage(prefix + "§cPlease enter a valid percentage!");
							return false;
						}
						api.startResize(p, percentage);
					}

					if (args.length == 4) {
						int width;
						int height;
						try {
							width = Integer.parseInt(args[2]);
							height = Integer.parseInt(args[3]);
						} catch (NumberFormatException e) {
							try {
								width = (int) scriptEngine.eval(args[2]);
								height = (int) scriptEngine.eval(args[3]);
							} catch (Exception e1) {
								sender.sendMessage(prefix + "§cPlease enter valid coordinates!");
								return false;
							}
						}

						api.startResize(p, width, height);
					}
					return api.startFrameCreation(p, url);
				}
				if (args[0].equalsIgnoreCase("remove")) {
					if (!sender.hasPermission("animatedframes.remove")) {
						sender.sendMessage(prefix + "§cNo permission.");
						return false;
					}

					if (removingFrame.contains(p.getUniqueId())) {
						sender.sendMessage(prefix + "§cYou are already removing a frame!");
						return false;
					}
					return api.startFrameRemoval(p);
				}
				if (args[0].equalsIgnoreCase("info")) {
					if (!sender.hasPermission("animatedframes.info")) {
						sender.sendMessage(prefix + "§cNo permission.");
						return false;
					}

					AnimatedImage theImage = null;
					for (Entity ent : p.getWorld().getEntitiesByClass(ItemFrame.class)) {
						if (ent instanceof ItemFrame) {
							for (AnimatedImage img : frames) {
								if (img.baseFrame == ent) {
									theImage = img;
									break;
								}
							}
						}
						if (theImage != null) {
							break;
						}
					}

					if (theImage == null) {
						sender.sendMessage(prefix + "§cUnable to find a valid screen!");
						return false;
					}

					sender.sendMessage(prefix + "§aFound a valid screen!");

					sender.sendMessage("        ");
					sender.sendMessage(prefix + "§b================");

					sender.sendMessage(prefix + "§bImage: §a" + theImage.imageSource);
					sender.sendMessage(prefix + "§bScreen size: §a" + theImage.width + "§bx§a" + theImage.height + " §b(§a" + theImage.width / 128 + "§bx§a" + theImage.height / 128 + "§b)");
					sender.sendMessage(prefix + "§bAnimation frame count: §a" + theImage.length);
					sender.sendMessage(prefix + "§bCurrent animation frame: §a" + theImage.currentFrame);
					sender.sendMessage(prefix + "§bAVG. animation frame delay: §a" + theImage.frameDelay + "ms");
					sender.sendMessage(prefix + "§bAmount of occupied item frames: §a" + theImage.getRequiredLength());

					sender.sendMessage(prefix + "§b================");

					return true;
				}
				if (args[0].equalsIgnoreCase("screen")) {
					if (args.length >= 2) {
						if (args[1].equalsIgnoreCase("generate") || args[1].equalsIgnoreCase("gen")) {
							if (!sender.hasPermission("animatedframes.screen.generate")) {
								sender.sendMessage(prefix + "§cNo permission.");
								return false;
							}
							if (args.length == 4) {
								int width;
								int height;
								try {
									width = Integer.parseInt(args[2]);
									height = Integer.parseInt(args[3]);
								} catch (NumberFormatException e) {
									try {
										width = (int) scriptEngine.eval(args[2]);
										height = (int) scriptEngine.eval(args[3]);
									} catch (Exception e1) {
										sender.sendMessage(prefix + "§cPlease enter valid numbers!");
										return false;
									}
								}
								Block targetBlock = p.getTargetBlock((Set<Material>) null, 4);
								if (targetBlock == null || targetBlock.getType() == Material.AIR) {
									sender.sendMessage(prefix + "§cUnable to find valid target block!");
									return false;
								}
								BlockFace face = targetBlock.getFace(p.getEyeLocation().getBlock());
								if (face == null) {
									sender.sendMessage(prefix + "§cPlease stand directly in front of the wall.");
									return false;
								}

								int modX = 0;
								int modZ = 0;
								if (face == BlockFace.WEST) {
									modX = 0;
									modZ = 1;
								} else if (face == BlockFace.SOUTH) {
									modX = 1;
									modZ = 0;
								} else if (face == BlockFace.EAST) {
									modX = 0;
									modZ = -1;
								} else if (face == BlockFace.NORTH) {
									modX = -1;
									modZ = 0;
								}

								List<Location> validLocations = new ArrayList<>();
								for (int x = 0; x < width; x++) {
									for (int z = 0; z < width; z++) {
										for (int y = 0; y < height; y++) {
											Location currLoc = targetBlock.getLocation().add(x * modX, y, z * modZ);
											Block currBlock = currLoc.getBlock();
											if (currBlock == null || currBlock.getType() == Material.AIR) {
												sender.sendMessage(prefix + "§cCan not spawn item frame @" + currLoc.getBlockX() + "," + currLoc.getBlockY() + "," + currLoc.getBlockZ() + ": Block is not solid!");
												return false;
											}
											validLocations.add(currLoc);
										}
									}
								}
								int count = 0;
								int fails = 0;
								for (Location loc : validLocations) {
									try {
										ItemFrame frame = loc.getWorld().spawn(loc.getBlock().getLocation().add(face.getModX(), 0, face.getModZ()), ItemFrame.class);
										frame.setFacingDirection(face, true);
										count++;
									} catch (Exception e) {
										fails++;
									}
								}
								sender.sendMessage(prefix + "§aGenerated " + count + " item frame(s)!");
								if (fails != 0) {
									sender.sendMessage(prefix + "§cFailed to generate " + fails + " frame(s).");
								}
								sender.sendMessage(prefix + "§7Re-join the server if you can't see any changes.");
								return true;
							} else {
								sender.sendMessage(prefix + "§c/animatedframes screen generate <width> <height>");
								return false;
							}
						}
						if (args[1].equalsIgnoreCase("clear")) {
							if (args.length == 3) {
								if (!sender.hasPermission("animatedframes.screen.clear")) {
									sender.sendMessage(prefix + "§cNo permission.");
									return false;
								}
								int radius;
								try {
									radius = Integer.parseInt(args[2]);
								} catch (NumberFormatException e) {
									sender.sendMessage(prefix + "§cPlease enter a valid number!");
									return false;
								}
								sender.sendMessage(prefix + "§7Searching for item frames in a radius of " + radius + "...");
								List<ItemFrame> frames = new ArrayList<>();
								int usedFrames = 0;
								entities:
								for (Entity ent : p.getNearbyEntities(radius, radius, radius)) {
									if (ent instanceof ItemFrame) {
										for (AnimatedImage img : AnimatedFrames.this.frames) {
											if (img.baseFrame.equals(ent)) {
												usedFrames++;
												continue entities;
											}
										}
										frames.add((ItemFrame) ent);
									}
								}
								if (usedFrames > 0) {
									sender.sendMessage(prefix + "§cFound " + usedFrames + " item frame(s) that are used for animations! Please remove them manually.");
								}
								if (frames.isEmpty()) {
									sender.sendMessage(prefix + "§cUnable to find nearby item frames.");
									return false;
								}
								sender.sendMessage(prefix + "§aFound " + frames.size() + " item frame(s) to remove.");
								for (ItemFrame frame : frames) {
									frame.remove();
								}
								sender.sendMessage(prefix + "§aRemoved all nearby frames!");
								return true;
							} else {
								sender.sendMessage(prefix + "§c/animatedframes screen clear <radius>");
								return false;
							}
						} else {
							sender.sendMessage(prefix + "§c/animatedframes screen <generate|clear>");
							return false;
						}
					} else {
						sender.sendMessage(prefix + "§c/animatedframes screen <generate|clear>");
						return false;
					}
				}
				sender.sendMessage(prefix + "§c/animatedframes <create|remove|screen|reload>");

				if ("reload".equalsIgnoreCase(args[0])) {
					if (!sender.hasPermission("animatedframes.reload")) {
						sender.sendMessage("§cNo permission");
						return false;
					}
					sender.sendMessage("§eReloading configuration...");
					reload();
					sender.sendMessage("§aReloaded.");
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();

		if (args.length == 1) {
			if (sender.hasPermission("animatedframes.create")) {
				list.add("create");
			}
			if (sender.hasPermission("animatedframes.remove")) {
				list.add("remove");
			}
			if (sender.hasPermission("animatedframes.screen.generate") || sender.hasPermission("animatedframes.screen.clear")) {
				list.add("screen");
			}
		}
		if (args.length == 2) {
			if ("screen".equalsIgnoreCase(args[0])) {
				if (sender.hasPermission("animatedframes.screen.generate")) {
					list.add("generate");
				}
				if (sender.hasPermission("animatedframes.screen.clear")) {
					list.add("clear");
				}
			}
		}

		return list;
	}

	@EventHandler
	public void onInteract(final PlayerInteractEntityEvent e) {
		if (e.getRightClicked() instanceof ItemFrame) {
			if (AFTimings.enabled()) { AFTimings.INTERACT_PROTECT.startTiming(); }
			if (e.getRightClicked().hasMetadata("AnimatedFrames")) {
				if (!e.getPlayer().isSneaking()) {
					e.setCancelled(true);
				} else {
					if (!e.getPlayer().hasPermission("animatedframes.modify")) {
						e.setCancelled(true);
					}
				}
			}
			if (AFTimings.enabled()) { AFTimings.INTERACT_PROTECT.stopTiming(); }
			if (e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().getType() != Material.AIR) { return; }
			if (e.getPlayer().hasPermission("animatedframes.create")) {
				if (AFTimings.enabled()) { AFTimings.INTERACT_CREATE.startTiming(); }
				if (!creatingFrame.isEmpty() && creatingFrame.containsKey(e.getPlayer().getUniqueId())) {
					e.setCancelled(true);

					URL url = creatingFrame.get(e.getPlayer().getUniqueId());
					creatingFrame.remove(e.getPlayer().getUniqueId());

					e.getPlayer().sendMessage(prefix + "§aCreating new frame...");

					try {

						final AnimatedImage img = new AnimatedImage(url);
						if (injectingMeta.containsKey(e.getPlayer().getUniqueId())) {
							img.meta = injectingMeta.remove(e.getPlayer().getUniqueId());
						}
						img.setCallback(new StatusCallback() {

							@Override
							public void onLoadFailed(List<Exception> reasons) {
								e.getPlayer().sendMessage(prefix + "§cFailed to load image:");
								for (Exception ex : reasons) {
									e.getPlayer().sendMessage(prefix + "§7" + ex.getMessage());
								}
								e.getPlayer().sendMessage(prefix + "§cSee console for details.");
							}

							@Override
							public void onImageLoaded() {
								e.getPlayer().sendMessage(prefix + "§aImage loaded. Calculating size...");

								if (resizeFrame.containsKey(e.getPlayer().getUniqueId())) {
									Dimension dim = resizeFrame.get(e.getPlayer().getUniqueId());
									if (dim.getHeight() != -1) {
										e.getPlayer().sendMessage(prefix + "§7Resizing image to " + dim.width + "x" + dim.height + ".");
									} else {
										e.getPlayer().sendMessage(prefix + "§7Resizing image to " + dim.width + "%");
									}
									img.resize(dim);
									resizeFrame.remove(e.getPlayer().getUniqueId());
								}

								ItemFrame[][] frames = new ItemFrame[img.width / 128][img.height / 128];

								e.getPlayer().sendMessage(prefix + "§aSearching for nearby frames...");

								int counter = 0;

								int shiftX = 0;
								int shiftZ = 0;

								switch (((ItemFrame) e.getRightClicked()).getFacing()) {
									case NORTH:
										shiftX = -1;
										e.getPlayer().sendMessage(prefix + "§7Creating screen facing NORTH");
										break;
									case EAST:
										shiftZ = -1;
										e.getPlayer().sendMessage(prefix + "§7Creating screen facing EAST");
										break;
									case SOUTH:
										shiftX = 1;
										e.getPlayer().sendMessage(prefix + "§7Creating screen facing SOUTH");
										break;
									case WEST:
										shiftZ = 1;
										e.getPlayer().sendMessage(prefix + "§7Creating screen facing WEST");
										break;
									// $CASES-OMITTED$
									default:
										e.getPlayer().sendMessage(prefix + "§cInavlid direction!");
										return;
								}

								for (int x = 0; x < img.width / 128; x++) {
									for (int y = 0; y < img.height / 128; y++) {
										for (Entity ent : e.getPlayer().getWorld().getEntitiesByClass(ItemFrame.class)) {
											if (ent instanceof ItemFrame) {
												if (ent.getLocation().getBlockZ() == e.getRightClicked().getLocation().getBlockZ() + shiftZ * x) {
													if (ent.getLocation().getBlockX() == e.getRightClicked().getLocation().getBlockX() + shiftX * x) {
														if (ent.getLocation().getBlockY() == e.getRightClicked().getLocation().getBlockY() + y) {
															frames[x][y] = (ItemFrame) ent;
															counter++;
															break;
														}
													}
												}
											}
										}
									}
								}

								if (counter < 1) {
									e.getPlayer().sendMessage(prefix + "§cCould not find a valid frame!");

									img.interrupt();
									return;
								}
								e.getPlayer().sendMessage(prefix + "§aFound §b" + counter + " §avalid frames.");

								img.setItemFrames(frames);

								e.getPlayer().sendMessage(prefix + "§7Screen requires around " + img.getRequiredLength() + " item frames (before optimizing).");

								if (img.getRequiredLength() > Short.MAX_VALUE) {
									e.getPlayer().sendMessage(prefix + "§cThe screen requires more frames than the short data type is able to contain! Aborting.");

									img.interrupt();
									return;
								}

								e.getPlayer().sendMessage(prefix + "§7Now optimizing animation. See console for progress information.");

								if (img.init()) {
									AnimatedFrames.this.frames.add(img);

									e.getPlayer().sendMessage(prefix + "§aCreated screen! Playing animation.");

									img.playAnimation();
								} else {
									e.getPlayer().sendMessage(prefix + "§cCould not create screen. See console for details.");
								}
							}
						});

					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				if (AFTimings.enabled()) { AFTimings.INTERACT_CREATE.stopTiming(); }
			}
			if (e.getPlayer().hasPermission("animatedframes.remove")) {
				if (AFTimings.enabled()) { AFTimings.INTERACT_REMOVE.startTiming(); }
				if (!removingFrame.isEmpty() && removingFrame.contains(e.getPlayer().getUniqueId())) {
					AnimatedImage toRemove = null;
					for (AnimatedImage img : frames) {
						if (img.baseFrame.equals(e.getRightClicked())) {
							toRemove = img;
							break;
						}
					}
					if (toRemove == null) {
						e.getPlayer().sendMessage(prefix + "§cUnable to find screen containing this frame!");
						removingFrame.remove(e.getPlayer().getUniqueId());
						return;
					}
					e.getPlayer().sendMessage(prefix + "§aFound valid screen. Removing...");

					toRemove.stopAnimation();
					toRemove.clearFrames();
					toRemove.interrupt();

					// Remove entity metadata
					for (int[] iA : toRemove.getItemFrameIDs()) {
						for (int i : iA) {
							for (Entity ent : toRemove.baseFrame.getWorld().getEntitiesByClass(ItemFrame.class)) {
								if (ent.getEntityId() == i) {
									if (ent.hasMetadata("AnimatedFrames")) {
										ent.removeMetadata("AnimatedFrames", AnimatedFrames.instance);
									}
								}
							}
						}
					}

					frames.remove(toRemove);

					e.getPlayer().sendMessage(prefix + "§aRemoved frame.");

					removingFrame.remove(e.getPlayer().getUniqueId());
				}
				if (AFTimings.enabled()) { AFTimings.INTERACT_REMOVE.stopTiming(); }
			}
		}

	}

	@EventHandler
	public void onJoin(final PlayerJoinEvent e) {
		final Player p = e.getPlayer();

		Bukkit.getScheduler().runTaskLaterAsynchronously(this, new Runnable() {
			@Override
			public void run() {
				loadingWrapper.getController().addViewer(p);
				loadingWrapper.getController().sendContent(p);

				for (final AnimatedImage img : frames) {
					img.setLoadingFor(p);
					//					for (MapWrapper[][] wrappers1 : img.mapWrappers) {
					//						for (MapWrapper[] wrappers : wrappers1) {
					for (MapWrapper wrapper : /*wrappers*/img.mapWrappers) {
						wrapper.getController().addViewer(p);
						wrapper.getController().sendContent(p);
					}
					//						}
					//					}
				}
			}
		}, 10);
	}

	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof ItemFrame) {
			if (e.getDamager() instanceof Player) {
				// for (AnimatedImage img : frames) {
				// if (img.baseFrame.equals(e.getEntity()) || img.isInChunk(e.getEntity().getLocation().getChunk())) {
				if (e.getEntity().hasMetadata("AnimatedFrames")) {
					if (!((Player) e.getDamager()).isSneaking()) {
						e.setCancelled(true);
					} else {
						if (!((Player) e.getDamager()).hasPermission("animatedframes.modify")) {
							e.setCancelled(true);
						}
					}
				}
				// break;
				// }
				// }
			}
		}
	}

	@EventHandler
	public void onHangingBreakByEntity(HangingBreakByEntityEvent e) {
		if (e.getEntity() instanceof ItemFrame) {
			if (e.getRemover() instanceof Player) {
				// for (AnimatedImage img : frames) {
				// if (img.baseFrame.equals(e.getEntity()) || img.isInChunk(e.getEntity().getLocation().getChunk())) {
				if (e.getEntity().hasMetadata("AnimatedFrames")) {
					if (!((Player) e.getRemover()).isSneaking()) {
						e.setCancelled(true);
					} else {
						if (!((Player) e.getRemover()).hasPermission("animatedframes.modify")) {
							e.setCancelled(true);
						}
					}
				}
				// break;
				// }
				// }
			} else {
				// for (AnimatedImage img : frames) {
				// if (img.baseFrame == e.getEntity()) {
				if (e.getEntity().hasMetadata("AnimatedFrames")) {
					e.setCancelled(true);
				}
				// break;
				// }
				// }
			}
		}
	}

	@EventHandler
	public void onHangingBreak(HangingBreakEvent e) {
		if (e.getEntity() instanceof ItemFrame) {
			// for (AnimatedImage img : frames) {
			// if (img.baseFrame == e.getEntity()) {
			if (e.getEntity().hasMetadata("AnimatedFrames")) {
				e.setCancelled(true);
			}
			// break;
			// }
			// }
		}
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent e) {
		final Player p = e.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(this, new Runnable() {
			@Override
			public void run() {
				//				for (AnimatedImage img : frames) {
				//					img.sendMaps(p);
				//				}
				for (AnimatedImage img : frames) {
					//					for (MapWrapper[][] wrappers1 : img.mapWrappers) {
					//						for (MapWrapper[] wrappers : wrappers1) {
					for (MapWrapper wrapper : /*wrappers*/img.mapWrappers) {
						wrapper.getController().addViewer(p);
						wrapper.getController().sendContent(p);
					}
					//						}
					//					}
				}
			}
		});
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(this, new Runnable() {
			@Override
			public void run() {
				for (AnimatedImage img : frames) {
					for (MapWrapper wrapper : /*wrappers*/img.mapWrappers) {
						wrapper.getController().addViewer(player);
						wrapper.getController().sendContent(player);
					}
				}
			}
		});
	}

	// Currently disabled, should already be fixed by canceling the chunk-unload
	// @EventHandler
	// public void onChunkLoad(ChunkLoadEvent e) {
	// for (AnimatedImage img : frames) {
	// if (img.baseFrame.getLocation().getChunk().equals(e.getChunk())) {
	// for (Player p : Bukkit.getOnlinePlayers()) {
	// if (p.getLocation().distance(e.getChunk().getBlock(0, 0, 0).getLocation()) < 16 * 16) {
	// img.sendMaps(p);
	// }
	// }
	// }
	// }
	// }

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChunkUnload(ChunkUnloadEvent e) {
		for (AnimatedImage img : frames) {
			if (img.baseFrame.getLocation().getChunk().equals(e.getChunk()) || img.isInChunk(e.getChunk())) {
				e.setCancelled(true);
				if (console_chunk) {
					getLogger().info("Cancelled chunk unloading @" + img.baseFrame.getLocation());
				}
			}
		}
	}

	public boolean saveFrames() {
		if (AFTimings.enabled()) { AFTimings.SAVE.startTiming(); }

		List<String> toDelete = new ArrayList<>();

		for (File file : new File(this.getDataFolder(), "/frames").listFiles()) {
			if (file.getName().endsWith(".af")) {
				toDelete.add(file.getAbsolutePath());
			}
		}

		getLogger().info("Saving frames..");
		for (AnimatedImage img : frames) {
			try {
				File file = img.saveToFile(new File(this.getDataFolder(), "/frames"));
				getLogger().info("Saved " + file.getAbsolutePath());
				toDelete.remove(file.getAbsolutePath());
			} catch (Exception e) {
				getLogger().log(Level.SEVERE, "Exception while saving frame", e);
			}
		}

		getLogger().info("Deleting old frames...");
		for (String s : toDelete) {
			getLogger().info("Deleting " + s);
			new File(s).delete();
		}

		if (AFTimings.enabled()) { AFTimings.SAVE.stopTiming(); }
		return true;
	}

	public boolean loadFrames() {
		if (AFTimings.enabled()) { AFTimings.LOAD.startTiming(); }
		getLogger().info("Loading frames...");
		for (final File file : new File(this.getDataFolder(), "/frames").listFiles()) {
			if (file.getName().endsWith(".af")) {
				try {

					BufferedReader reader = new BufferedReader(new FileReader(file));

					String jsonString = "";
					String line = null;
					while ((line = reader.readLine()) != null) {
						jsonString += line;
					}

					reader.close();

					final JSONObject json = new JSONObject(jsonString);

					URL url = new URL(json.getString("source"));
					int width0 = -1;
					int height0 = -1;
					if (json.has("width") && json.has("height")) {
						width0 = json.getInt("width");
						height0 = json.getInt("height");
					}
					JSONObject meta = null;
					if (json.has("meta")) {
						meta = json.getJSONObject("meta");
					}

					ImageLoadEvent.LoadCallback callback = new ImageLoadEvent.LoadCallback() {
						@Override
						public void call(final URL url, final int width, final int height, final JSONObject meta) {
							try {
								getLogger().info("Loading file: " + file);

								final AnimatedImage img = new AnimatedImage(url);
								if (meta != null) { img.meta = meta; }
								img.setCallback(new StatusCallback() {

									@Override
									public void onLoadFailed(List<Exception> reasons) {
									}

									@Override
									public void onImageLoaded() {

										if (width != -1 && height != -1) {
											Dimension dim = new Dimension(width, height);
											img.resize(dim);
										}

										final ItemFrame[][] frames = new ItemFrame[img.width / 128][img.height / 128];

										BlockFace face = BlockFace.valueOf(json.getString("face"));

										JSONObject locObj = json.getJSONObject("baseLocation");
										final Location loc = new Location(Bukkit.getWorld(locObj.getString("world")), locObj.getInt("x"), locObj.getInt("y"), locObj.getInt("z"));

										int sX = 0;
										int sZ = 0;

										switch (face) {
											case NORTH:
												sX = -1;
												break;
											case EAST:
												sZ = -1;
												break;
											case SOUTH:
												sX = 1;
												break;
											case WEST:
												sZ = 1;
												break;
											// $CASES-OMITTED$
											default:
												return;
										}

										final int shiftX = sX;
										final int shiftZ = sZ;

										Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {

											@Override
											public void run() {
												loc.getChunk().load();
												loc.clone().add(16, 0, 0).getChunk().load();
												loc.clone().add(-16, 0, 0).getChunk().load();
												loc.clone().add(0, 0, 16).getChunk().load();
												loc.clone().add(0, 0, -16).getChunk().load();

												int counter = 0;

												for (int x = 0; x < img.width / 128; x++) {
													for (int y = 0; y < img.height / 128; y++) {
														entity:
														for (Entity ent : loc.getWorld().getEntitiesByClass(ItemFrame.class)) {
															if (ent instanceof ItemFrame) {
																if (ent.getLocation().getBlockZ() == loc.getBlockZ() + shiftZ * x) {
																	if (ent.getLocation().getBlockX() == loc.getBlockX() + shiftX * x) {
																		if (ent.getLocation().getBlockY() == loc.getBlockY() + y) {
																			frames[x][y] = (ItemFrame) ent;
																			counter++;
																			break entity;
																		}
																	}
																}
															}
														}
													}
												}

												if (counter < 1) {
													img.interrupt();
													return;
												}

												img.setItemFrames(frames);

												Bukkit.getScheduler().runTaskAsynchronously(instance, new Runnable() {
													@Override
													public void run() {
														if (img.init()) {
															img.playAnimation();

															AnimatedFrames.this.frames.add(img);
														}
													}
												});
											}
										});

									}
								});
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					};

					ImageLoadEvent event = new ImageLoadEvent(url, width0, height0, meta, callback);
					Bukkit.getPluginManager().callEvent(event);

					if (ImageLoadEvent.getPendingCallbacks() <= 0) {//There are no registered load listeners
						event.finish();
					}
				} catch (Exception e) {
					System.err.println("[AnimatedFrames] Exception while loading frame: " + e.getMessage());
					e.printStackTrace(System.err);
				}
			}
		}
		if (AFTimings.enabled()) { AFTimings.LOAD.stopTiming(); }
		return true;
	}

	static boolean ArrayContains(int[] array, int value) {
		for (int t : array) {
			if (t == value) { return true; }
		}
		return false;
	}

	class API implements AnimatedFramesAPI {

		@Override
		public void injectMeta(@Nonnull Player player, @Nonnull JSONObject meta) {
			injectingMeta.put(player.getUniqueId(), meta);
		}

		@Override
		public boolean startResize(@Nonnull Player player, @Nonnull @Nonnegative int width, @Nonnull @Nonnegative int height) {
			if (width < 128 || height < 128) {
				player.sendMessage(prefix + "§cScreen size must be at least 128x128!");
				return false;
			}

			if (width % 128 != 0 || height % 128 != 0) {
				player.sendMessage(prefix + "§cWidth and height must be a multiple of 128!");
				return false;
			}

			Dimension dim = new Dimension(width, height);
			resizeFrame.put(player.getUniqueId(), dim);
			return false;
		}

		@Override
		public boolean startResize(@Nonnull Player player, @Nonnull @Nonnegative int percentage) {
			Dimension dim = new Dimension(percentage, -1);
			resizeFrame.put(player.getUniqueId(), dim);
			return true;
		}

		@Override
		public boolean startFrameCreation(@Nonnull Player player, @Nonnull URL url) {
			try {
				url = URLHelper.discoverRealSource(url);
			} catch (Exception e2) {
				player.sendMessage(prefix + "§cCould not find real image source! See console for details.");
				e2.printStackTrace();
			}
			player.sendMessage(prefix + "§7Image source: " + url.toString());

			boolean nearbyFrame = false;
			for (Entity ent : player.getNearbyEntities(8, 8, 8)) {
				if (ent instanceof ItemFrame) {
					nearbyFrame = true;
					break;
				}
			}
			if (!nearbyFrame) {
				player.sendMessage(prefix + "§cPlease make sure you have created your frame-screen first!");
				return false;
			}

			creatingFrame.put(player.getUniqueId(), url);
			player.sendMessage(prefix + "§aPlease now right-click the bottom-left frame of your screen.");
			return true;
		}

		@Override
		public boolean startFrameRemoval(@Nonnull Player player) {
			removingFrame.add(player.getUniqueId());

			player.sendMessage(prefix + "§aPlease now right-click the bottom-left frame of your screen.");
			return true;
		}

		@Override
		public boolean isCreating(@Nonnull Player player) {
			return creatingFrame.containsKey(player.getUniqueId());
		}

		@Override
		public boolean isRemoving(@Nonnull Player player) {
			return removingFrame.contains(player.getUniqueId());
		}

		@Override
		public boolean isResizing(@Nonnull Player player) {
			return resizeFrame.containsKey(player.getUniqueId());
		}

	}

}
