/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.animatedframes;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.inventivetalent.animatedframes.event.PlayerImageVisibilityEvent;
import org.inventivetalent.animatedframes.images.GifDecoder;
import org.inventivetalent.mapmanager.MapManagerPlugin;
import org.inventivetalent.mapmanager.controller.MapController;
import org.inventivetalent.mapmanager.controller.MultiMapController;
import org.inventivetalent.mapmanager.manager.MapManager;
import org.inventivetalent.mapmanager.wrapper.MapWrapper;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class AnimatedImage extends Thread/* implements org.inventivetalent.animatedframes.api.Image*/ {

	public static int SINGLE_IMAGE_DELAY = 2500;

	private StatusCallback callback;

	private GifDecoder decoder;
	private   int imageType = BufferedImage.TYPE_INT_ARGB;
	protected int length    = 0;
	protected int width     = 128;
	protected int height    = 128;

	protected MapWrapper[] mapWrappers = new MapWrapper[0];
	protected int[] frameDelays;

	protected int currentFrame = 0;

	protected int frameDelay = 0;                    // Average

	private boolean loadingImage = false;
	private boolean playing      = false;

	protected String      imageSource;
	private   InputStream imageIn;

	protected ItemFrame baseFrame;
	private int[][] frameIDs = new int[0][0];

	private Dimension imageSize;

	public JSONObject meta;

	public List<Exception> loadExceptions = new ArrayList<>();

	public List<UUID> loadingPlayers = new CopyOnWriteArrayList<>();

	@SuppressWarnings("resource")
	public AnimatedImage(File file) throws Exception {
		this(new FileInputStream(file));

		this.imageSource = file.toString();

		this.setName(this.getName() + " [" + file.toString() + "]");
	}

	public AnimatedImage(URL url) throws Exception {
		this(openURLConnection(url).getInputStream());

		this.imageSource = url.toString();

		this.setName(this.getName() + " [" + url.toString() + "]");
	}

	private AnimatedImage(final InputStream in) throws Exception {
		this.imageIn = in;
		this.loadingImage = true;

		this.setName("AnimatedImage");

		this.start();
	}

	public void setCallback(StatusCallback callback) {
		this.callback = callback;
	}

	public void resize(Dimension dim) {
		if (dim.height != -1) {
			this.imageSize = dim;
			this.width = dim.width;
			this.height = dim.height;
		} else {
			this.width = this.width * dim.width / 100;
			this.height = this.height * dim.width / 100;
			this.imageSize = new Dimension(this.width, this.height);
		}
	}

	public int getRequiredLength() {
		return this.length * this.width / 128 * (this.height / 128);
	}

	public boolean init() {
		int width = this.width / 128;
		int height = this.height / 128;

		System.out.println("[AnimatedFrames] Generating images...");

		frameDelays = new int[decoder.getFrameCount()];
		int nullCount = 0;
		for (int i = 0; i < frameDelays.length; i++) {
			frameDelays[i] = decoder.getDelay(i);
			if (frameDelays[i] == 0) {
				System.err.println("[AnimatedFrames] Frame #" + i + " has a delay of 0.");
				nullCount++;
			}
		}
		if (nullCount > (frameDelays.length / 2)) {
			System.err.println("[AnimatedFrames] More than 50% of the animation has a frame delay of 0.");
			System.err.println("[AnimatedFrames] Cancelling creation. Proceeding would freeze your server.");
			return false;
		}

		this.mapWrappers = new MapWrapper[this.length];

		MapManager mapManager = ((MapManagerPlugin) Bukkit.getPluginManager().getPlugin("MapManager")).getMapManager();
		for (int i = 0; i < this.length; i++) {
			BufferedImage current = this.decoder.getFrame(i);
			decoder.frames.set(i, null);
			if (current == null) {
				continue;
			}
			BufferedImage image = new BufferedImage(128 * width, 128 * height, this.imageType);
			Graphics g = image.getGraphics();
			if (current.getWidth() != 128 * width || current.getHeight() != 128 * height) {
				Image scaled = current.getScaledInstance(128 * width, 128 * height, Image.SCALE_FAST);
				g.drawImage(scaled, 0, 0, null);
				scaled.flush();
			} else {
				g.drawImage(current, 0, 0, null);
			}
			g.dispose();
			current.flush();

			try {
				this.mapWrappers[i] = mapManager.wrapMultiImage(image, height, width);

				for (Player player : Bukkit.getOnlinePlayers()) {
					setLoadingFor(player);

					this.mapWrappers[i].getController().addViewer(player);
					this.mapWrappers[i].getController().sendContent(player);
				}

			} catch (Exception e) {
				System.err.println("[AnimatedFrames] Exception while splitting image: " + e.getMessage());
				e.printStackTrace();
			}
			image.flush();
		}

		decoder = null;

		return true;
	}

	public void setItemFrames(ItemFrame[][] frames) {
		this.baseFrame = frames[0][0];
		this.frameIDs = new int[frames.length][frames[0].length];
		for (int x = 0; x < frames.length; x++) {
			for (int y = 0; y < frames[x].length; y++) {
				if (frames[x][y] != null) {
					this.frameIDs[x][y] = frames[x][y].getEntityId();
					frames[x][y].setMetadata("AnimatedFrames", new FixedMetadataValue(AnimatedFrames.instance, (byte) 1));
				} else {
					System.err.println("ItemFrame @" + x + "," + y + " is null!");
				}
			}
		}
	}

	public int[][] getItemFrameIDs() {
		return this.frameIDs;
	}

	public void playAnimation() {
		this.playing = true;
	}

	public void stopAnimation() {
		if (!this.playing) { return; }
		this.playing = false;

		for (MapWrapper wrapper :/* wrappers2*/ this.mapWrappers) {
			wrapper.getController().clearViewers();
		}

		MapManager mapManager = ((MapManagerPlugin) Bukkit.getPluginManager().getPlugin("MapManager")).getMapManager();
		for (MapWrapper wrapper :/* wrappers2*/ this.mapWrappers) {
			mapManager.unwrapImage(wrapper);
		}
	}

	@SuppressWarnings({
			"rawtypes",
			"unchecked",
			"deprecation" })
	@Override
	public void run() {
		if (this.loadingImage) {
			this.loadingImage = false;

			this.decoder = new GifDecoder();
			this.decoder.read(this.imageIn);

			this.length = this.decoder.getFrameCount();
			if (this.length <= 0) {
				try {
					System.out.println("[AnimatedFrames] No frames found. Using single image with a delay of " + SINGLE_IMAGE_DELAY + " instead.");
					this.length = 1;
					this.decoder.frameCount = 1;
					BufferedImage img = null;
					try {
						img = ImageIO.read(new URL(this.imageSource));
					} catch (Exception e) {
						loadExceptions.add(e);
						try {
							img = ImageIO.read(new File(this.imageSource));
						} catch (Exception e1) {
							loadExceptions.add(e1);
							try {
								img = ImageIO.read(this.imageIn);
							} catch (Exception e2) {
								loadExceptions.add(e2);
							}
						}
					}
					if (img == null) {
						System.err.println("[AnimatedFrames] Failed to load image!");

						for (Exception e : loadExceptions) {
							System.out.println(" \r\n");
							System.out.println(e.getMessage());
							e.printStackTrace();
						}

						this.callback.onLoadFailed(this.loadExceptions);
						return;
					}
					loadExceptions.clear();

					this.imageType = img.getType();
					this.decoder.frames.add(new GifDecoder.GifFrame(img, SINGLE_IMAGE_DELAY));
					Dimension size = new Dimension(img.getWidth(), img.getHeight());
					this.width = size.width;
					this.height = size.height;
					this.imageSize = size;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (this.imageSize == null) {
				Dimension size = this.decoder.getFrameSize();
				this.width = size.width;
				this.height = size.height;
				this.imageSize = size;
			}

			try {
				this.imageIn.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			int totalDelay = 0;
			for (int i = 0; i < this.length; i++) {
				totalDelay += this.decoder.getDelay(i);
			}
			this.frameDelay = this.length > 0 ? totalDelay / this.length : 0;

			this.callback.onImageLoaded();
		}

		while (!this.playing) {
			try {
				sleep(100);
			} catch (InterruptedException e) {
				System.err.println("[AnimatedFrames] The delay until playing the animation has been interrupted: " + e.getMessage());
				e.printStackTrace();
			}
		}

		while (this.playing) {
			int delay = this.frameDelays[currentFrame];

			if (Bukkit.getOnlinePlayers().isEmpty()) {
				try {
					sleep(1000);
				} catch (InterruptedException e) {
				}
				continue;
			}

			MultiMapController controller = ((MultiMapController) this.mapWrappers[this.currentFrame].getController());
			for (final Player p : Bukkit.getOnlinePlayers()) {
				if (this.isVisibleToPlayer(p)) {
					if (AnimatedFrames.instance.loadingImage && !this.loadingPlayers.isEmpty() && this.loadingPlayers.contains(p.getUniqueId())) {
						//Show Loading Image
						MapController controller1 = AnimatedFrames.instance.loadingWrapper.getController();
						for (int x = 0; x < this.frameIDs.length; x++) {
							for (int y = 0; y < this.frameIDs[x].length; y++) {
								controller1.showInFrame(p, this.frameIDs[x][y], "§7This Image is still downloading...");
							}
						}
					} else {
						if (AnimatedFrames.instance.frameDebug) {
							controller.showInFrames(p, this.frameIDs, new MultiMapController.DebugCallable() {
								@Override
								public String call(MapController controller1, int x, int y) {
									return "§7#" + currentFrame + " " + x + "|" + y + " @" + controller1.getMapId(p);
								}
							});
						} else {
							controller.showInFrames(p, this.frameIDs);
						}
					}
				}
			}

			this.currentFrame++;

			if (this.currentFrame >= this.length) {
				this.currentFrame = 0;
			}
			try {
				sleep(delay);
			} catch (InterruptedException e) {
				System.err.println("[AnimatedFrames] The animation thread has been interrupted. Animation was probably stopped manually: " + e.getMessage());
			}
		}

	}

	@SuppressWarnings({
			"rawtypes",
			"unchecked",
			"deprecation" })
	public void clearFrames() {
		for (int i = 0; i < this.length; i++) {
			MultiMapController controller = ((MultiMapController) this.mapWrappers[i].getController());
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (this.isVisibleToPlayer(p)) {
					controller.clearFrames(p, this.frameIDs);
				}
			}
		}
	}

	public void setLoadingFor(final Player player) {
		if (!AnimatedFrames.instance.loadingImage) { return; }
		if (loadingPlayers.contains(player.getUniqueId())) { return; }
		loadingPlayers.add(player.getUniqueId());
		Bukkit.getScheduler().runTaskLater(AnimatedFrames.instance, new Runnable() {
			@Override
			public void run() {
				loadingPlayers.remove(player.getUniqueId());
			}
		}, ((MapManager.Options.Sender.DELAY * getRequiredLength()) / MapManager.Options.Sender.AMOUNT));
	}

	protected boolean isVisibleToPlayer(Player p) {
		if (p.getWorld() != this.baseFrame.getWorld()) { return false; }
		boolean visible = this.baseFrame.getLocation().distance(p.getEyeLocation()) < AnimatedFrames.instance.animationDistance;

		PlayerImageVisibilityEvent event = new PlayerImageVisibilityEvent(p, this, this.baseFrame.getLocation(), visible);
		Bukkit.getPluginManager().callEvent(event);

		if (event.isCancelled()) { return false; }

		return visible;
	}

	public boolean isInChunk(Chunk chunk) {
		for (Entity ent : chunk.getEntities()) {
			if (ent instanceof ItemFrame) {
				for (int[] iA : this.frameIDs) {
					if (AnimatedFrames.ArrayContains(iA, ent.getEntityId())) { return true; }
				}
			}
		}
		return false;
	}

	static URLConnection openURLConnection(URL url) throws IOException {
		URLConnection connection = url.openConnection();
		connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		connection.setDoOutput(true);
		connection.setDoInput(true);
		return connection;
	}

	protected File saveToFile(File folder) throws Exception {
		File file = new File(folder, "frame#" + this.hashCode() + ".af");
		JSONObject json = new JSONObject();

		json.put("source", this.imageSource);
		json.put("width", this.width);
		json.put("height", this.height);
		json.put("baseLocation", new JSONObject() {
			{
				Location loc = AnimatedImage.this.baseFrame.getLocation();
				this.put("world", loc.getWorld().getName());
				this.put("x", loc.getBlockX());
				this.put("y", loc.getBlockY());
				this.put("z", loc.getBlockZ());
			}
		});
		json.put("face", this.baseFrame.getFacing().name());
		if (meta != null) {
			json.put("meta", meta);
		}

		System.out.println("[AnimatedFrames] Saving file: " + file);

		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(json.toString(2));
		writer.flush();
		writer.close();

		return file;
	}

	//
	//	@Override
	//	public ItemFrame getBaseFrame() {
	//		return baseFrame;
	//	}
	//
	//	@Override
	//	public int[][] getFrameIds() {
	//		return frameIDs;
	//	}
	//
	//	@Override
	//	@Deprecated
	//	public short[][][] getContainingIds() {
	//		return new short[0][0][0];
	//	}
	//	//		return containingIDs;
	//	//	}
	//
	//	@Override
	//	public int[] getFrameDelays() {
	//		return frameDelays;
	//	}
	//
	//	@Override
	//	public boolean isPlaying() {
	//		return playing;
	//	}
	//
	//	@Override
	//	public String getImageSource() {
	//		return imageSource;
	//	}
	//
	//	@Override
	//	public JSONObject getMeta() {
	//		return meta;
	//	}
	//
	//	@Override
	//	public int getLength() {
	//		return length;
	//	}
	//
	//	@Override
	//	public int getWidth() {
	//		return width;
	//	}
	//
	//	@Override
	//	public int getHeight() {
	//		return height;
	//	}
}
