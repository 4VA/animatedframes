/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.animatedframes;

import org.bukkit.entity.Player;
import org.json.JSONObject;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.net.URL;

public interface AnimatedFramesAPI {

	/**
	 * Adds metadata to be saved in the animation file
	 *
	 * @param player Target player (about to create the frame)
	 * @param meta   {@link JSONObject} meta
	 */
	void injectMeta(@Nonnull Player player, @Nonnull JSONObject meta);

	/**
	 * Starts the resizing process for a player
	 *
	 * @param player Player
	 * @param width  Width to resize to
	 * @param height Height to resize to
	 * @return <code>true</code> if resizing is successful, <code>false</code> if one of the arguments is invalid
	 */
	boolean startResize(@Nonnull Player player, @Nonnull @Nonnegative int width, @Nonnull @Nonnegative int height);

	/**
	 * Starts the resizing process for a player
	 *
	 * @param player     Player
	 * @param percentage Percentage to resize to
	 * @return <code>true</code> if resizing is successful, <code>false</code> if one of the arguments is invalid
	 */
	boolean startResize(@Nonnull Player player, @Nonnull @Nonnegative int percentage);

	/**
	 * Starts the creation of a frame
	 *
	 * @param player Player
	 * @param url    Image url
	 * @return <code>true</code> if successful, <code>false</code> if one of the arguments is invalid
	 */
	boolean startFrameCreation(@Nonnull Player player, @Nonnull URL url);

	/**
	 * Starts the removal of a frame
	 *
	 * @param player Player
	 * @return <code>true</code> if successful, <code>false</code> if one of the arguments is invalid
	 */
	boolean startFrameRemoval(@Nonnull Player player);

	/**
	 * @param player Player
	 * @return <code>true</code> if the player is creating a frame
	 */
	boolean isCreating(@Nonnull Player player);

	/**
	 * @param player Player
	 * @return <code>true</code> if the player is resizing a frame
	 */
	boolean isResizing(@Nonnull Player player);

	/**
	 * @param player Player
	 * @return <code>true</code> if the player is removing a frame
	 */
	boolean isRemoving(@Nonnull Player player);

}
